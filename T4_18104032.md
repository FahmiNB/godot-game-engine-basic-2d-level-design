# Hasil observasi saya mengenai pengenalan game engine GODOT
## Matereri Basic 2D Level Design, menambah **Fitur Tambahan**
### Menambah Asset 
- `Door`(pintu Castle)
- Backround `Level 1` dan `Level`
- Logo Pemberitahuan `Level 1` dan `Level 2` pada _Respawn_ `Player` 
- Screen Akhir #DIRUMAHAJA

### Menambah Animation

Dari praktikum ini saya menambahkan _**Animation**_ di node `Player` pada child `Animator` pada bar bawah animator saya menambahkan _Animation_ `Joged` dan menambahkan code sendiri dengan browsing permasalahan saat posisi `Idle`(diam) tidak bisa di gabung dengan _Animation_ apapun sehingga saya bisa menggunakan _Animation_ saat `Idle`. Code nya sebagai berikut, pada node `Player` -> Scripts `Player.gd`. Akan joged ketika **Menekan Keyboard 'P'**

```
extends KinematicBody2D

export (int) var speed = 400
export (int) var GRAVITY = 1200
export (int) var jump_speed = -400

const UP = Vector2(0,-1)


var velocity = Vector2()

onready var animator = self.get_node("Animator")
onready var sprite = self.get_node("Sprite")


func get_input():
	velocity.x = 0
	if is_on_floor() and Input.is_action_just_pressed('jump'):
		velocity.y = jump_speed
	if Input.is_action_pressed('right'):
		velocity.x += speed
	if Input.is_action_pressed('left'):
		velocity.x -= speed

			
func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)

func _process(delta):
	if velocity.y != 0:
		animator.play("Jump")
	elif velocity.x != 0:
		animator.play("Walk")
		if velocity.x > 0:
			sprite.flip_h = false
		else:
			sprite.flip_h = true
	elif $Animator.current_animation == "Joget" and $Animator.is_playing():
			pass
	else:
		$Animator.play("Idle")
	
func _input(event):
	if event is InputEventKey and event.pressed:
		if event.scancode == KEY_P:
			get_node("Animator") .play("Joget")
```

## Lebih detailnya pada line **(code paling bawah)**
```
elif $Animator.current_animation == "Joget" and $Animator.is_playing():
			pass
	else:
		$Animator.play("Idle")
	
func _input(event):
	if event is InputEventKey and event.pressed:
		if event.scancode == KEY_P:
			get_node("Animator") .play("Joget")
```


### Menambah Level 2
Ketika player menabrak `Area Trigger` akan berpindah ke `level 2`. Karena pada `Area Triger` terdapat `CollisionShape2D` yang memiliki objek transparan yang bisa di tabrak, dan pada `Area 2D` yang di _rename_ menjadi `Area Trigger` terdapat code yang memindah `Scane` tergantung dengan nama `Scane`, pada code bisa di patenkan di `ScaneName`.


Ketika Player masuk ke `Door` `Castle` terdapat `Area Triger` pada pintu dan berpindah ke `Level 2`. Pada `Level 2` kesulitan betambah dan mencari `Treasure`(harta karun) di `Castle` ketika `Player` menyentuh `Treasure`(Harta karun) tersebut akan berpindak ke Win Screen karena `Area Trigger` mempunyai nama `Win Screen`.

Dengan #DIRUMAHAJA

### Menambah background dan kreatifan TileMap
Pada Background saya menambahkan gambar penmandangan salju terdapat `Castle` dengan `TileMap` berwana putih untuk menyusaikan suasana dan keterangan Logo Level 1 pada _Respawn_ `Player` pada `Level` yang bisa di masuki Melaluin _node child_ `Door`.

Selanjutnya pada level 2 pada awal `Player` _Respawn_ terdapat logo keterangn Level 2. Terdapat _background_ tembok `Castle` yang biru gelap seperti sudah menyelimuti salju yang dingin, `TileMap` pada `Level 2` sedikit lebih susah karena semakin menambah levelnya akan semakin susah. Dan terdapat `Treasure` untuk di dapatkan dan Game selesai akan mnuju ke `Win Screen`#DIRUMAHAJA 


